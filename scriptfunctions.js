function startPagina() {
    if (application.projectid == 60) {
        document.getElementById('QC_kanaal').value = "A";
        gotopage('A01_Aanvang');
    } else if (application.projectid == 66) {
        document.getElementById('QC_kanaal').value = "D";
        gotopage('D01_Aanvang');
    } else if (application.projectid == 67) {
        document.getElementById('QC_kanaal').value = "F";
        gotopage('F01_Aanvang');
    } else if (application.projectid == 1125) {
        document.getElementById('QC_kanaal').value = "R";
        gotopage('R01_Receptie');
    } else {
        document.getElementById('QC_kanaal').value = "A";
        gotopage('A01_Aanvang');
    }
}

function determineChannel() {
    if (document.getElementById('QC_kanaal').value === "A" || document.getElementById('QC_kanaal').value === "F") {
        return fulfilment.mail("tintelingen@qualitycontacts.nl", "vragen@tintelingen.nl", "QC Telefoonnotitie van gebruiker: " + application.userid + " omtrent: " + data.field('vraag'), "A89_FF_Notitie", "", "smtp.office365.com", "587", "tintelingen@qualitycontacts.nl", "C8AGoppFWJL8i1Wxc9HD")

    } else if (document.getElementById('QC_kanaal').value === "D") {
        return fulfilment.mail("tintelingen@qualitycontacts.nl", "vragen@tintelingen.nl", "QC Telefoonnotitie van gebruiker: " + application.userid + " omtrent: " + data.field('vraag') + " Defensie", "A89_FF_Notitie_Defensie", "", "smtp.office365.com", "587", "tintelingen@qualitycontacts.nl", "C8AGoppFWJL8i1Wxc9HD");

    }

}

function docReady(fn) {
    // see if DOM is already available
    if (
        document.readyState === "complete" ||
        document.readyState === "interactive"
    ) {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
} // docReady

function cleanParam(param) {
    var ret = param;
    if (ret.indexOf("$") !== -1) {
        ret = ret.replace(/\$/g, "");
    }

    return ret;
} // cleanParam

function getRelevantGreeting() {
    var time = new Date();
    var hour = time.getHours();
    var ret = "goedenacht";
    if (hour >= 6 && hour < 12) {
        ret = "goedemorgen";
    } else if (hour >= 12 && hour < 18) {
        ret = "goedemiddag";
    } else if (hour >= 18) {
        ret = "goedenavond";
    }
    return ret;
}

function getRelevantGreetingCapitalized() {
    let greeting = getRelevantGreeting();
    if (greeting.length >= 2) {
        greeting = greeting[0].toUpperCase() + greeting.slice(1);
    };

    return greeting;
}

/**
 * getDynamicText
 */
function getDynamicText(param) {
    var ret = "";

    switch (param) {
        case "userfullname":
            var userfullname = application.userfullname;
            ret = userfullname;
            break;
        case "userfirstname":
            var userfirstname = application.userfullname.split(' ')[0];
            ret = userfirstname;
            break;
        case "goededagdeel":
            ret = getRelevantGreeting();
            break;
        case "Goededagdeel":
            ret = getRelevantGreetingCapitalized();
            break;
        default:
            var val = data.field(param);
            if (typeof val === undefined || val === null || val === "undefined") {
                val = "";
            }
            ret = val;
            break;
    }
    return ret;
} // getDynamicText

/**
 * processParam
 */
function processParam(param) {
    var main = document.getElementsByTagName("main")[0];
    var ret = cleanParam(param);
    var fullParam = "$" + ret + "$";

    _mainStr = _mainStr.replace(fullParam, getDynamicText(ret));
    main.innerHTML = _mainStr;
} // processParam

/**
 * replaceDynamicText
 */
function replaceDynamicText() {
    var main = document.getElementsByTagName("main")[0];
    _mainStr = main.innerHTML;
    var params = _mainStr.match(/\$(.*?)\$/g);

    if (params !== null) {
        for (var i = 0; i < params.length; i++) {
            var param = params[i];
            processParam(param);
        }
    }
} // replaceDynamicText

// FAQ functionaliteit
function faqEventListeners() {

    let links = document.getElementsByClassName("faqLink");
    for (let i = 0; i < links.length; i++) {
        links[i].addEventListener("click", detClick)
    }

}

function detClick() {
    let content = $(".faq-content");
    content.hide();

    if (this.id === "jsFaqInloggenLink") {
        $("#jsFaqInloggen").show();
    }

    if (this.id === "jsFaqGeschenkAlgemeenLink") {
        $("#jsFaqGeschenkAlgemeen").show();
    }

    if (this.id === "jsFaqBelevenissenLink") {
        $("#jsFaqComingSoon").show();
    }

    if (this.id === "jsFaqGiftcardsLink") {
        $("#jsFaqGiftcards").show();
    }
    if (this.id === "jsFaqGoedeDoelenLink") {
        $("#jsFaqGoedeDoelen").show();
    }

}

function validateInput() {
    let inlogCode = document.getElementById("inlogcode");
    let shopId = document.getElementById("shopid");
    let inputAlert = $("#inputAlert");

    document.getElementById("notitieOpslaan").addEventListener("click", function () {
        if (shopId.value == "" && inlogCode.value == "") {
            inputAlert.show();
            inputAlert.html("<p>Vul de vereiste gegevens in:" + "<br>" + "ShopID" + "<br>" + "Inlogcode </p>");
        } else if (shopId.value != "" && inlogCode.value == "") {
            inputAlert.show();
            inputAlert.html("<p>Vul de vereiste gegevens in:" + "<br>" + "Inlogcode </p>")
        } else if (shopId.value == "" && inlogCode.value != "") {
            inputAlert.show();
            inputAlert.html("<p>Vul de vereiste gegevens in:" + "<br>" + "ShopID </p>")
        } else {
            inputAlert.hide();
        }
    })
}

function showData(){
    let dataSet = JSON.parse(relateddata("Bonnen").json);
    alert(dataSet);
}
// Zoekfunctionaliteit
function filldatatable(tablename, dataname){
	var dataSet = JSON.parse(window.external.relateddata(dataname).json);
	if ( ! $.fn.DataTable.isDataTable( '#' + tablename ) ) {
		var table = $('#' + tablename).dataTable({"data": dataSet,"columns": [

			{ "data": "ID", "defaultContent": "" },
            { "data": "CouponNummer", "defaultContent": "" },
            { "data": "AuthorizationCode", "defaultContent": "" },
            { "data": "Naam_1", "defaultContent": "" },
            { "data": "Postcode", "defaultContent": "" }],
			"columnDefs": [{"targets": [0],"visible": false}],
				//"order": [[ 0, "desc" ]],	
				"paging": false,"searching": false,"bInfo" : false
				});     
		
		$('#' + tablename + ' tbody').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
                relateddata(dataname).currentrecord=relateddata(dataname).findrecord('ID',dtselectedid(tablename));
				Knop_Verder.disabled=false;
			}
			else {
				table.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
                relateddata(dataname).currentrecord=relateddata(dataname).findrecord('ID', dtselectedid(tablename));
				Knop_Verder.disabled=false;
			}
		})
			
	} else {
		var table = $('#' + tablename).dataTable().api();
	
		table.clear();
		table.rows.add(dataSet);
		table.draw();
	};
}

function dtselectedid(tablename){
	var table = $('#' + tablename).DataTable();
	var tID = table.cell(table.row('.selected').index(),0).data();
	if (tID > 0) {
		return(tID);
	}
}

function jump(){
    var tableid = relateddata('Table').field('Table_Id');
    var recordid = relateddata('Search').field('ID');
        application.gotorecord(tableid, recordid);
}
function addSearchEvents(){
  let btn = document.getElementById("btnSearch");

  btn.addEventListener("click", function(){
    let subjectField = document.getElementById("lstSearch");
    if (subjectField.value == ""){
      $("#listFeedback").show();
    } else {
      $("#listFeedback").hide();
      relateddata("Search").getdata(lstSearch.value + " LIKE '%" + txtSearch.value + "%'",true);
    }
  })
}

// Einde zoekfunctionaliteit